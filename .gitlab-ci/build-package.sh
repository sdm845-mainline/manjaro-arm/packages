#!/bin/bash

echo "building '$1'"

cd "$1"

sudo -u "$BUILD_USER" makepkg --config "../.gitlab-ci/makepkg-arm64.conf" -C -Ascf --noconfirm --log
