#!/bin/bash

set -e
set -o pipefail


find_changed() {
    ( [[ "$CI_BUILD_ALL" != "TRUE" ]] && git diff --name-only -z "$1" 2>/dev/null ) ||
    (
        echo "Returning all packages!" 1>&2
        find . -type f -name PKGBUILD -mindepth 2 -maxdepth 2 -print0
    )
}

last_commit="${1:=HEAD^}"

echo "finding changed files since ${last_commit}:" 1>&2
find_changed "$last_commit" \
  | xargs -r -0 dirname -z \
  | sort -u -z \
  | (grep -v -z -e '^\.git' -e '^\.$' || true)
